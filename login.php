<?php session_start();

    if (isset($SESSION['usuario'])) {
        header('location: index.php');
    }

    $error="";

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        
        $usuario=$_POST['usuario'];
        $clave=$_POST['clave'];
        $clave = hash('sha512', $clave); 

    if(empty($clave) or empty($usuario)){
        $error .='<i>Favor de rellenar todos los campos</i>';
    }else{
 
        try{
            $conexion =new PDO('mysql:host=localhost;dbname=pp','root','DianaLucy1997');
        }catch(PDOException $prueba_error){
            echo "Error:" .$prueba_error->getMessage();
        }

        $statement =$conexion ->prepare('
        SELECT usuario FROM login WHERE usuario = :usuario AND clave =:clave'
        );
        
        $statement->execute(array(
            ':usuario'=> $usuario,
            ':clave'=> $clave
        ));
        
        $resultado =$statement->fetch();

        if ($resultado !== false) {
            $_SESSION['usuario'] = $usuario;
            header('location: principal.php');            
        }else{

            $statement =$conexion ->prepare('
            SELECT usuario FROM login WHERE usuario = :usuario'
            );
            $statement->execute(array(
                ':usuario'=> $usuario
            ));  
            $resultado =$statement->fetch();
            if ($resultado==true) {
                $error .= '<i> La constraseña es incorrecta</i>';
            }else{
                $error .= '<i> Este usuario no existe</i>';
            }
        }
    }
    }

    require 'frontend/login-vista.php';
    
?>