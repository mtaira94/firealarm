<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Proyectos Productivos</title>
    <link rel="shortcut icon" href="img/icon.png">
	<link rel='stylesheet' type='text/css' media='screen' href='css/style-menu.css'>
    <link rel='stylesheet' type='text/css' media='screen' href='css/icon/style.css'>
    <!-- <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">  -->
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet"> 
    <script src="js/jquery.js"></script>    
    <script src="js/script-menu.js"></script> 
</head>
<body>

<div class="header"></div>

<header > <Span class="lnr lnr-menu  show"> </span></header> 
      
    
<main>
    <div class="content-menu">
        <ul class="menu">
            <li><a href="principal.php"><span class="icon1 lnr lnr-home"> </span><h4 class="text">Inicio   </h4></a></li>
            <li><a href="notificacion.php"><span class="icon2 lnr lnr-phone-handset">   </span><h4 class="text">Notificaciones  </h4></a></li> 
            <li><a href="grafica.php"><span class="icon3 lnr lnr-chart-bars">       </span><h4 class="text">Graficas     </h4></a></li>
            <li><a href="#"><span class="icon4 lnr lnr-cog">         </span><h4 class="text">Configuracion</h4></a></li> 
            <li><a href="#"><span class="icon5 lnr lnr-book">            </span><h4 class="text">Manual  </h4></a></li>  
            <a href="cerrar.php"><li><span class="icon6 lnr lnr-exit">    </span><h4 class="text">Cerrar sesion</h4></li></a> 
         </ul> 
    </div>
    
    <article  class="article"> 
        
         <img src="img/logo.png" alt="LogoGobiernoDelEstado">
       
        <footer>    </footer>
    </article>

    
</main>




</body>
</html>