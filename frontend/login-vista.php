<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Proyectos Productivos</title>
    <link rel="shortcut icon" href="img/icon.png">
	<link rel='stylesheet' type='text/css' media='screen' href='css/style-login.css'>
    <link rel='stylesheet' type='text/css' media='screen' href='css/icon/style.css'>
    
</head>
<body>
    
<div class="container-form">
    <div class="header">
        <div class="logo-title"> 
            
        </div>
        <div class="menu">
            <a href="login.php">
                <li class="module-login" style="border-bottom: 4px solid #1550FF;" >Login</li></a>
           
        </div>
    </div>
        <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>"
        method="post" class="form">
        
            <div class="welcome-form"><h1>Bienvenido</h1><h2>Fire Alarm</h2></div>
            
            <div class="username line-input">
                <label class="lnr lnr-user"></label>
                <input type="text" placeholder="Nombre Usuario" name="usuario">
            </div>
            <div class="password line-input">
                <label class="lnr lnr-lock"></label>
                <input type="password" placeholder="Contraseña" name="clave">
            </div>
           
                <?php if(!empty($error)): ?>
            <div class="mensaje">
                <?php echo $error; ?>
            </div>
                <?php endif; ?>
            
                <button type="submit">Entrar <label class="lnr lnr-chevron-right"></label></button>
        </form>   
</div>

<script src="js/jquery.js"></script>
<script src="js/script-log.js"></script>

</body>